﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Partial.Startup))]
namespace Partial
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
