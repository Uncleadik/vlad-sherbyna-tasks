﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(cacl.Startup))]
namespace cacl
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
