﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace cacl.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Приложение калькулятор";
            return View();
        }

        [HttpPost]
        public ActionResult Index(decimal a, decimal b, string act)
        {
            ViewBag.Message = "Результат";
            ViewBag.Result = Models.Calculation.calcAct(a, b, act);
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}