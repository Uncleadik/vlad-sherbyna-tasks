﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cacl.Models
{
    public class Calculation
    {
        public static string calcAct(decimal a, decimal b, string act)
        {
            decimal summ = 0;
            if (act == "1")
                summ = a + b;
            else if (act == "2")
                summ = a - b;
            else if (act == "3")
                summ = a * b;
            else if (act == "4")
                summ = a / b;
            return summ.ToString();
        }
    }
}