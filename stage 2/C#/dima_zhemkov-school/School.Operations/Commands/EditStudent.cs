﻿using System;
using System.Linq;
using School.Common.Core;
using School.Data.Entity.Models;
using School.Data.Interfaces;
using School.Operations.Attributes;
using School.Operations.Interfaces;
using School.Operations.Helpers;

namespace School.Operations.Commands
{
    [CommandName("edit-student")]
    [CommandDescription("edit-student", typeof(Strings))]
    public sealed class EditStudent : ICommand, IDisposable
    {
        private readonly IStudentRepository _repository;

        public EditStudent(IStudentRepository repository)
        {
            _repository = repository;
        }

        public void Execute()
        {
            var students = _repository.GetAll().ToArray();

            students.PrintPersonList(Strings.AllStudents);

            var student = students.PickPerson(Strings.PickStudentToEdit);

            Console.WriteLine(Strings.EnterStudentsFirtsName + " ({0})",student.FirstName);
            student.FirstName = Console.ReadLine();
            Console.WriteLine(Strings.EnterStudentsLastName + " ({0})", student.LastName);
            student.LastName = Console.ReadLine();
            Console.WriteLine(Strings.EnterStudentsAge + " ({0})", student.Age);
            int age;
            while (!int.TryParse(Console.ReadLine(), out age))
            {
                Console.WriteLine(Strings.InvalidAgeValue);
            }

            student.Age = age;

            Console.ReadKey();

            _repository.Update(student);
            _repository.SaveChanges();
        }

        public void Dispose()
        {
            _repository.Dispose();
        }
    }
}
