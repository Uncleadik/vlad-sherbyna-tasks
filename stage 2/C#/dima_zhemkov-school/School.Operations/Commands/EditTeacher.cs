﻿using System;
using School.Common.Core;
using School.Data.Entity.Models;
using School.Data.Interfaces;
using School.Operations.Attributes;
using School.Operations.Interfaces;
using System.Linq;
using School.Operations.Helpers;

namespace School.Operations.Commands
{
    [CommandName("edit-teacher")]
    [CommandDescription("edit-teacher", typeof(Strings))]
    public sealed class EditTeacher : ICommand, IDisposable
    {
        private readonly ITeacherRepository _repository;

        public EditTeacher(ITeacherRepository repository)
        {
            _repository = repository;
        }

        public void Execute()
        {
            var teachers = _repository.GetAll().ToArray();

            teachers.PrintPersonList(Strings.AllTeachers);

            var teacher = teachers.PickPerson(Strings.PickTeacherToEdit);

            Console.WriteLine(Strings.EnterTeachersFirtsName + " ({0})", teacher.FirstName);
            teacher.FirstName = Console.ReadLine();
            Console.WriteLine(Strings.EnterTeachersLastName + " ({0})", teacher.LastName);
            teacher.LastName = Console.ReadLine();
            Console.WriteLine(Strings.EnterTeachersAge + " ({0})", teacher.Age);
            int age;
            while (!int.TryParse(Console.ReadLine(), out age))
            {
                Console.WriteLine(Strings.InvalidAgeValue);
            }

            teacher.Age = age;

            Console.ReadKey();

            _repository.Update(teacher);
            _repository.SaveChanges();
        }

        public void Dispose()
        {
            _repository.Dispose();
        }
    }
}
