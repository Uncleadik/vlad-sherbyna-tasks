$(document).ready(function() {

	// Слайдер

    $(window).load(function(){
      $('.flexslider').flexslider({
        animation: "slide",
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });

    // Меню

	$(".main_mnu_button").click(function() {
		$(".maian_mnu ul").slideToggle();
	});


	// ПОРТФОЛИО РАБОТ
	// 
	// 

	$("#portfolio_grid").mixItUp();

	$(".portfolio li").click(function() {
		$(".portfolio li").removeClass("active");
		$(this).addClass("active");
	});
	

	// Модальные окна

	 $('.image-link').magnificPopup({type:'image'});

	 $('.test-popup-link').magnificPopup({
  		type: 'image'
	});

	//Кнопка "Вниз"

	$(".scroll").click(function() {
 	 	$("html, body").animate({ scrollTop: $(document).height() }, 800);
  		return false;
	});



	//Кнопка "Наверх"

	$(".to-top").click(function () {
		$("body").animate({
			scrollTop: 0
		}, 800);
		return false;
	});
	
	



});