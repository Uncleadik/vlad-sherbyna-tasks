var app = angular.module('app', ['ui.router']);

// Роутинг **************

app.config(function ($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise('/home')

	$stateProvider.state('home', {
		url: '/home',
		templateUrl: 'views/home.html'
	})

	.state('posts', {
		url: '/posts',
		templateUrl: 'views/posts.html'
	})

	.state('posts.post1', {
		url: '/post1',
		templateUrl: 'views/post1.html'
	})

	.state('posts.post2', {
		url: '/post2',
		templateUrl: 'views/post2.html'
	});
});


// Контроллер ****************


app.controller('toDoList', function ($scope, $filter) {


$scope.itemList = [  // поля по умолчанию
	{name: 'Ivan', surname: 'Ivanov', age: 36, Email: 'ivan.gmail.com', gender: 'Мужской', done:false},
	{name: 'Petr', surname: 'Petrov', age: 42, Email: 'petr.gmail.com', gender: 'Мужской', done:false},
	{name: 'Vika', surname: 'Chaikova', age: 25, Email: 'vika.gmail.com', gender: 'Женский', done:false},
];

// Добавить пользователя

$scope.addItem = function () {
	$scope.itemList.push({	name: $scope.name,
							surname: $scope.surname,
							age: $scope.age,
							Email: $scope.Email,
							gender: $scope.gender,
							done:false	}); // добавляем елемент
	$scope.name = " ";
	$scope.surname = " ";
	$scope.age = " ";
	$scope.Email = " "; 
	$scope.gender = " "; // обновляем поле

	var jsons = JSON.stringify($scope.itemList);//переобразовываем объект в массив состаящий из строк
    localStorage.setItem("jsons", jsons);

};

// Удалить пользователя

$scope.delItem = function () {
	var oldList = $scope.itemList;
    $scope.itemList = [];
    angular.forEach(oldList, function(x) {
        if (x.done == false)
        	$scope.itemList.push(x);
    });
};

// Фильтр

    $scope.dataSelect = {
          model: '',
          availableOptions: [
            {value: '', name: 'Все'},
            {value: 'Мужской', name: 'Мужчины'},
            {value: 'Женский', name: 'Женщины'}
          ]
        };

// Количество выбраных елементов

	$scope.$watch('itemList', function () {
		$scope.itemCount = $filter('filter')($scope.itemList, {done: true}).length;
	}, true)

});
