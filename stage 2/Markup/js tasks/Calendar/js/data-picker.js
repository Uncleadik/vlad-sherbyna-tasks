var htmlCalendar = '<div id="content"><ul id="calendar-freebie"><li><div id="calendar"><div class="c-title dark-gray-3level">' +
						       '<div class="left-button">&lsaquo;</div><div class="right-button">&rsaquo;</div><div id="calendar-year" class="title">' +
						       '</div></div><table class="c-days"><thead class="light-gray"><tr><th>Пн</th><th>Вт</th><th>Ср</th><th>Чт</th>' +
                   '<th>Пт</th><th>Сб</th><th>Вс</th></tr></thead><tbody></tbody></table></div></li></ul></div>';

window.onload = document.querySelector('.date-input').innerHTML += htmlCalendar;

document.querySelector('#drop-down-date').onclick = function(){
  if(document.querySelector('#content').style.display == 'none')
    document.querySelector('#content').style.display = 'table';
  else
    document.querySelector('#content').style.display = 'none';
}


var currentMonth = new Date().getMonth(),
    currentDay = new Date().getDate(),
    months= ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Окрябрь','Ноябрь','Декабрь'];

var cells;



function calendarGenerator(idYear, idCalendar, year, month, day){

var D = new Date(year, month, day),
    Dlast = new Date(D.getFullYear(),D.getMonth()+1,0).getDate(), // последний день месяца
    DNlast = new Date(D.getFullYear(),D.getMonth(),Dlast).getDay(), // день недели последнего дня месяца
    DNfirst = new Date(D.getFullYear(),D.getMonth(),1).getDay(), // день недели первого дня месяца
    calendarHtml = '<tr class="simple-lightgray">'; // название месяца, вместо цифр 0-11

// пустые клетки до первого дня текущего месяца
if (DNfirst != 0) {
  for(var  i = 1; i < DNfirst; i++) calendarHtml += '<td class="other-month"></td>';
}
else{ // если первый день месяца выпадает на воскресенье, то требуется 7 пустых клеток
  for(var  i = 0; i < 6; i++) calendarHtml += '<td class="other-month"></td>';
}



// дни месяца
for(var  i = 1; i <= Dlast; i++) {
  if (i != D.getDate()) {
    calendarHtml += '<td><span><a href="#">' + i + '</a></span></td>';
  }
  else{
    calendarHtml += '<td class="today"><span><a href="#">' + i + '</a></span></td>';  // сегодняшней дате можно задать стиль CSS
  }
  if (new Date(D.getFullYear(),D.getMonth(),i).getDay() == 0) {  // если день выпадает на воскресенье, то перевод строки
    calendarHtml += '<tr class="simple-lightgray">';
  }
}

// пустые клетки после последнего дня месяца
if (DNlast != 0) {
  for(var  i = DNlast; i < 7; i++) calendarHtml += '<td><span></span></td>';
}

document.querySelector('#' + idYear).innerHTML = months[D.getMonth()] + ' ' + D.getFullYear();
document.querySelector('#' + idCalendar + ' tbody').innerHTML = calendarHtml;
}

getDataFromMonth(new Date().getMonth(),new Date().getDate());


document.querySelector('.left-button').onclick =  function() {
  if(currentMonth == new Date().getMonth()+1){
    getDataFromMonth(--currentMonth,currentDay);
  }
  else{
    getDataFromMonth(--currentMonth,1);
  }
};


document.querySelector('.right-button').onclick =  function() {
  if(currentMonth == new Date().getMonth()-1){
    getDataFromMonth(++currentMonth,currentDay);
  }
  else{
    getDataFromMonth(++currentMonth,1);
  }
};

function getDataFromMonth(month, day){
  calendarGenerator('calendar-year','calendar',new Date().getFullYear(),month,day);
  cells = document.querySelectorAll('.c-days td');
  outputDate();
}

function logText() {
  var value = document.querySelector('#calendar-year').innerText.split(' ');
  var dropDownInput = document.querySelector('#drop-down-date');

  if(this.textContent.length == 2 && months.indexOf(value[0]) < 10)
    dropDownInput.value = this.textContent + '/0' + (months.indexOf(value[0]) + 1) + '/' + value[1];
  else if(this.textContent.length == 1 && months.indexOf(value[0]) < 9)
    dropDownInput.value = '0' + this.textContent + '/0' + (months.indexOf(value[0]) + 1) + '/' + value[1];
  else if(this.textContent.length == 1 && months.indexOf(value[0]) > 9)
    dropDownInput.value = '0' + this.textContent + '/' + (months.indexOf(value[0]) + 1) + '/' + value[1];
  else if(this.textContent.length == 2 && months.indexOf(value[0]) > 9)
    dropDownInput.value = this.textContent + '/' + (months.indexOf(value[0]) + 1) + '/' + value[1];
}

function outputDate() {
    Array.prototype.forEach.call(cells, function(td) {
    td.addEventListener('click', logText);
  });
}
