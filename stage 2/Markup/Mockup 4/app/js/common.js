$(document).ready(function() {

    // 1 Меню

	$(".main_mnu_button").click(function() {
		$(".maian_mnu ul").slideToggle();
	});

	$(".categoty-pick").click(function() {
		$(".hover-mnu-items-1").slideToggle();
	});


	// 2 Меню

	$(".second_mnu_button").click(function() {
		$(".second_mnu ul").slideToggle();
	});



	// Слайдер

  $('.top-slider').slick({
  	dots: true,
  	arrows: false,
  	infinite: true,
  	autoplay: true,
  	autoplaySpeed: 4000,
  	speed: 1000,
  	mobileFirst: true
  });


  $('.second-slider').slick({
  	dots: false,
  	arrows: false,
  	infinite: true,
    autoplay: true,
    autoplaySpeed: 4000,
    speed: 1000,
  	prevArrow: '<button type="button" class="left-img"><img src="img/arrows/3.png"></button>',
	nextArrow: '<button type="button" class="right-img"><img  src="img/arrows/4.png"></button>',
  	slidesToShow: 6,
  	centerMode: false,
  	responsive: [

    {
      breakpoint: 1180,
      settings: {
      	slidesToShow: 4,
        centerMode: false,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 3,
        centerMode: true,
        centerPadding: '30px',
        mobileFirst: true
      }
    },
     {
      breakpoint: 600,
      settings: {
       slidesToShow: 2,
        centerMode: true,
        centerPadding: '10px',
        mobileFirst: true
      }
    }
  ]
  });


  $('.third-slider').slick({
  	dots: false,
  	arrows: false,
  	infinite: true,
  	slidesToShow: 5,
  	centerMode: false,
    autoplay: true,
    autoplaySpeed: 4000,
    speed: 1000,
  	responsive: [

    {
      breakpoint: 1180,
      settings: {
      	slidesToShow: 5,
        centerMode: false,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 3,
        centerMode: true,
        centerPadding: '30px',
        mobileFirst: true
      }
    },
     {
      breakpoint: 600,
      settings: {
       slidesToShow: 2,
        centerMode: true,
        centerPadding: '30px',
        mobileFirst: true
      }
    }
  ]
  });


  $('.fourth-slider').slick({
  	dots: false,
  	arrows: false,
  	infinite: true,
  	slidesToShow: 4,
  	centerMode: false,
    autoplay: true,
    autoplaySpeed: 4000,
    speed: 1000,
  	responsive: [

    {
      breakpoint: 1180,
      settings: {
      	slidesToShow: 4,
        centerMode: false,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 3,
        centerMode: true,
        centerPadding: '30px',
        mobileFirst: true
      }
    },
     {
      breakpoint: 600,
      settings: {
       slidesToShow: 2,
        centerMode: true,
        centerPadding: '30px',
        mobileFirst: true
      }
    }
  ]
  });


  // Смена изображения

$(".my_image").bind("click", function() {
     var src = ($(this).attr("src") === "img/2slider/3.png")
                    ? "img/2slider/03.png" 
                    : "img/2slider/3.png";
      $(this).attr("src", src);
});

$(".plus").bind("click", function() {
      var src = ($(this).attr("src") === "img/plus.png")
                    ? "img/minus.png" 
                    : "img/plus.png";
      $(this).attr("src", src);
});

});