﻿/*
 * Created by SharpDevelop.
 * User: Feldmarchall
 * Date: 18.11.2016
 * Time: 21:23
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_9
{
	
	 class Program
    {
        static void Main(string[] args)
        {
            Person person = new Person("Иванов","Иванович");
            ManagmentPerson firstPerson = new ManagmentPerson();

            person.AddEvent += firstPerson.AddPersonHandler;

            person.InvokeAddEvent();

            person = new Person("Иванов", "Анатольевич");

            person.ChangeEvent += firstPerson.ChangePersonHandler;

            person.InvokeChangeEvent();

            person.DeleteEvent += firstPerson.DeletePersonHandler;

            person.InvokeDeleteEvent();

            person = null;
        }
        
        
              public void AddPersonHandler(string name, string lastName)
        {
            Console.WriteLine("Имя: {0}\nФамилия: {1}\n", name, lastName);
        }

        public void DeletePersonHandler(string name, string lastName)
        {
            Console.WriteLine("Удален: {0} {1}\n", name , lastName);
        }

        public void ChangePersonHandler(string name, string lastName)
        {
            Console.WriteLine("Изменен: {0} {1}\n", name, lastName);
        }
        
        
         public delegate void PersonStateChangeHandler(string name, string lastName);

        PersonStateChangeHandler addEvent = null;
        PersonStateChangeHandler changeEvent = null;
        PersonStateChangeHandler deleteEvent = null;

        public event PersonStateChangeHandler AddEvent
        {
            add { addEvent += value; }
            remove { addEvent -= value; }
        }

        public event PersonStateChangeHandler ChangeEvent
        {
            add { changeEvent += value; }
            remove { changeEvent -= value; }
        }

        public event PersonStateChangeHandler DeleteEvent
        {
            add { deleteEvent += value; }
            remove { deleteEvent -= value; }
        }

        public void InvokeAddEvent()
        {
            addEvent?.Invoke(this.name, this.lastName);
        }

        public void InvokeDeleteEvent()
        {
            deleteEvent?.Invoke(this.name, this.lastName);
        }

        public void InvokeChangeEvent()
        {
            changeEvent?.Invoke(this.name, this.lastName);
        }

        string name, lastName;

        public void Person(string name, string lastName)
        {
            this.name = name;
            this.lastName = lastName;
        }
        
    }
	

	

	

}