﻿/*
 * Created by SharpDevelop.
 * User: Feldmarchall
 * Date: 05.11.2016
 * Time: 8:20
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Thirt
{
	
	class Program
    {
        static void Main(string[] args)
        {
            Student[] student = new Student[4];

            Student firstStudent = new Student {firstName = "Игорь",lastName="Сычев",age = 15 };
            Student secondStudent = new Student { firstName = "Сергей", lastName = "Серов", age = 20 };
            Student thirdStudent = new Student { firstName = "Влад", lastName = "Игнатченко", age = 18 };
            Student fourthStudent = new Student { firstName = "Марк", lastName = "Будько", age = 17 };

            student[0] = firstStudent;
            student[1] = secondStudent;
            student[2] = thirdStudent;
            student[3] = fourthStudent;

            Array.Sort(student, (a, b) => { return a.firstName.CompareTo(b.firstName); });

            Console.WriteLine("\t\tСортировка по имени");
            foreach (var NameStudent in student)
                Console.WriteLine(NameStudent);

            Array.Sort(student,new StudentsComparer());

            Console.WriteLine("\t\tСортировка по возрасту");
            foreach (var AgeStudent in student)
                Console.WriteLine(AgeStudent);
            Console.ReadKey();
        }
    }
	
	
    struct Student
    {
        string _firstName;
        string _lastName;
        int _age;

        public string firstName
        {
            get
            {
                return _firstName;
            }
            set
            {
                _firstName = value;
            }
        }

        public string lastName
        {
            get
            {
                return _lastName;
            }
            set
            {
                _lastName = value;
            }
        }

        public int age
        {
            get
            {
                return _age;
            }
            set
            {         
                _age = value;
            }
        }

        public override string ToString()
        {
            return string.Format("Имя: {0}\nФамилия: {1}\nВозраст: {2}\n",_firstName, _lastName, _age);
        }

    }

    class StudentsComparer : IComparer<Student>
    {
        public int Compare(Student x, Student y)
        {
            if (x.age < y.age)
                return 1;
            if (x.age > y.age)
                return -1;
            else
                return 0;
        }
    }



}