﻿/*
 * Created by SharpDevelop.
 * User: Feldmarchall
 * Date: 04.11.2016
 * Time: 21:47
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace Second
{
	class Program
	{
		
		static void Main(string[] args)
        {
        	Console.WriteLine("Введите размер массива");
        	 n = Convert.ToInt16(Console.ReadLine());
        	 m = Convert.ToInt16(Console.ReadLine());
            Program instance = new Program();
			
            instance.initArr();
            instance.trans();
            instance.showArr("Массив до сортировки");
            Console.WriteLine("\nСумма  по столбцам");
            instance.sumArr();
            instance.sortArr();
            instance.showArr("\nМассив после сортировки");
            Console.ReadKey();
        }
		
    	static int n, m;
    	
        int[,] Array = new int[n, m];
        int[,] ModArray = new int[m, n];

        int[] sum = new int[m];


        int[,] initArr()
        {
            Random randomNumber = new Random();

                for (int i = 0; i < n; i++)
                    for (int j = 0; j < m; j++)
                        Array[i,j] = randomNumber.Next(-500, 500);


            return Array;
        }

        int[,] trans()
        {
            for (int i = 0; i < m; i++)
                for (int j = 0; j < n; j++)
                    ModArray[i, j] = Array[j, i];

            return ModArray;
        }

        int[] sumArr()
        {
            for (int i = 0; i < m; i++)
                for (int j = 0; j < n; j++)
                    sum[i] += ModArray[i, j];

            foreach (int x in sum)
                Console.Write(x + " ");

            Console.WriteLine();

            return sum;
        }

        void showArr(string message)
        {
            Console.WriteLine(message);

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Console.Write(Array[i, j] + " ");
                }
                Console.WriteLine();
            }
        }

        void bubbleSortSum()
        {
            for (int i = 0; i < m; i++)
                for (int j = 0; j < m - i - 1; j++)
                    if (sum[j] > sum[j + 1])
                    {
                        int temp = sum[j];
                        sum[j] = sum[j + 1];
                        sum[j + 1] = temp;
                    }
        }

        void sortArr()
        {
            int[] counter = new int[m];
            int[] copySum = new int[m];

            sum.CopyTo(copySum, 0);

            bubbleSortSum();

            for (int i = 0; i < m; i++)
                for (int j = 0; j < m; j++)
                    if (sum[i] == copySum[j])
                        counter[i] = j;

            for (int i = 0; i < m; i++)
                for (int j = 0; j < n; j++)
                    Array[j,i] = ModArray[counter[i], j];
        }


		
		
		
	}
}
