﻿/*
 * Created by SharpDevelop.
 * User: Feldmarchall
 * Date: 12.11.2016
 * Time: 7:44
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_4_4
{
	class Program
	{
		public static void Main(string[] args)
		{
		    Autos[] listAutos = new Autos[4];
		    listAutos[0] = new Autos("Ford",62347892,"Серов",2009,02,10,12678);
            listAutos[1] = new Autos("Ferrari",25728123,"Сычев",2010,11,03,267823);
            listAutos[2] = new Autos("Audi",72389137,"Игнатченко",1996,12,28,823137);
            listAutos[3] = new Autos("Toyota",26782378,"Ерохин",1999,06,28,376713);
            Array.Sort(listAutos, (a, b) => {return a.Kilometerss.CompareTo(b.Kilometerss);});

            foreach (var auto in listAutos)
                if(auto.dates.Year <2010)
                Console.WriteLine(auto);
			Console.ReadKey();
		}
	}
	
	
	   class Autos
    {
	   	int NumberAuto, Kilometers;
        string ModelOfAuto, lastName;
        public DateTime dates;

        public int NumberAutos
        {
            get
            {
                return NumberAuto;
            }
             set
            {

                 NumberAuto = value;
            }
        }

        public int Kilometerss
        {
            get
            {
                return Kilometers;
            }
            set
            {

                Kilometers = value;
            }
        }
        
        public string ModelOfAutos
        {
            get
            {
                return ModelOfAuto;
            }
            set
            {
                ModelOfAuto = value;
            }
        }

        public string LastNames
        {
            get
            {
                return lastName;
            }
            set
            {

                lastName = value;

            }
        }

        public Autos(string ModelAut, int NumberAut, string LastName, int year,int month,int day,  int Kilo)
        {
            this.ModelOfAutos = ModelAut;
            this.NumberAuto = NumberAut;
            this.LastNames = LastName;
            this.dates = new DateTime(year, month, day);
            this.Kilometerss = Kilo;
        }

        public override string ToString()
        {
            return string.Format("Марка машины: {0}\nНомер машины: {1}\nФамилия владельца: {2}\nПробег: {3} км\nГод приобретения: {4}\n", 
               ModelOfAuto, NumberAuto, lastName, Kilometers, dates.ToString("dd-MM-yyyy"));
        }
    }
	
}