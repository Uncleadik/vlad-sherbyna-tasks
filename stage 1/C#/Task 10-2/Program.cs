﻿/*
 * Created by SharpDevelop.
 * User: Feldmarchall
 * Date: 18.11.2016
 * Time: 21:48
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections;

namespace Task_10_2
{
  class Program
    {
        static void Main(string[] args)
        {
            ArrayList ArrNums = new ArrayList(){ 5, -1, 4, 0, 13, 10, -7, 15 };
            Console.WriteLine("Сортировать по возрастанию или по спаданию? \n Выберите 1 или 2");
            int choise = Convert.ToInt32( Console.ReadLine() );
            if (choise == 1)
            	ArrNums.ChangedSortUp();
            else
            	ArrNums.ChangedSortDown();      
            foreach (var elem in ArrNums)
                Console.Write(elem + " ");
            Console.ReadKey();
        }
    }
  
   public static class newMetods
    {
        public static void ChangedSortUp(this ArrayList list)
        {
            object temp;

                for (int i = 0; i < list.Count - 1; i++)
                {
                    for (int j = i + 1; j < list.Count; j++)
                    {
                        if ((int)list[i] > (int)list[j])
                        {
                            temp = list[i];
                            list[i] = list[j];
                            list[j] = temp;
                        }
                    }
                }
        }

        public static void ChangedSortDown(this ArrayList list)
        {
            object temp;
     
                for (int i = 0; i < list.Count - 1; i++)
                {
                    for (int j = i + 1; j < list.Count; j++)
                    {
                        if ((int)list[i] < (int)list[j])
                        {
                            temp = list[i];
                            list[i] = list[j];
                            list[j] = temp;
                        }
                    }
                }
        }
    }
  
}