﻿/*
 * Created by SharpDevelop.
 * User: Feldmarchall
 * Date: 12.11.2016
 * Time: 7:10
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace Task_3_1
{
	class Program
	{
		public static void Main(string[] args)
		{
		  TaskNums nums = new TaskNums();
            string res = nums.ResNums("H:\\Numbers.txt").ToString();
            
            Console.WriteLine(res);
            Console.ReadKey();
		}
	}
	
	 class TaskNums
    {

        string OpenSource = @"\d+\.\d+";
        string Line = "";
        double SumofNums = 0;

        public double ResNums(string path)
        {
            if (String.IsNullOrWhiteSpace(path))
            	return 0;           
            StreamReader reader = new StreamReader(@path);
            Regex IgnoreEmpt = new Regex(OpenSource,RegexOptions.IgnorePatternWhitespace);

            while (!reader.EndOfStream)
                Line += reader.ReadLine();

            string[] Str2 = Line.Split(' ');            

            	try
            		{
               			 foreach (string str in Str2)
                   			 if(IgnoreEmpt.IsMatch(str))
                        		SumofNums += double.Parse(IgnoreEmpt.Match(str).Value, CultureInfo.InvariantCulture);
            		}
            	catch (Exception err)
            		{
                		Console.WriteLine(err.Message);
                		return 0;
            		}

            	return SumofNums;
        }
    }
}