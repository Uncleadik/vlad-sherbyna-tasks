﻿/*
 * Created by SharpDevelop.
 * User: Feldmarchall
 * Date: 18.11.2016
 * Time: 22:20
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_11
{
	
	class Program
    {
        static void Main(string[] args)
        {
            Complex complex = new Complex(7.2623);

            double myNum = 12.123;
            complex = myNum;
            Console.WriteLine(myNum);
            Console.ReadKey();
        }
    }
	
    struct Complex
    {
        private double value;

        public Complex(double value)
        {
            this.value = value;
        }

        public static bool operator ==(Complex LeftValue,Complex RightValue)
        {
            return LeftValue.Equals(RightValue);
        }

        public static bool operator !=(Complex LeftValue, Complex RightValue)
        {
            return !LeftValue.Equals(RightValue);
        }

        public static implicit operator Complex(double arg)
        {
            Complex complex = new Complex(arg);
            return complex;
        }

        public override bool Equals(object obj)
        {
            if(obj is Complex)
                if (((Complex)obj).value == this.value)
                    return true;

            return false;
        }

        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("{0}",this.value);
        }
    }


}