﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskLink
{
    class Program
    {
        static void Main(string[] args)
        {
            LinqTasks instance = new LinqTasks();

            int[] ArrayOfNumbers = { 1, -2, 24, -12, 22, 5, -50, 3, -3, 23 };
            string[] ArrayOfStrings = {"Магаз", "abc", "4stor", "пошли", "гулять", "1метод", "а1а"};

            //Task 1

            int FirstPositive = instance.FirstPositive(ArrayOfNumbers);
            int LastNegative = instance.LastNegative(ArrayOfNumbers);

            Console.WriteLine("Наибольшее первое число : {0}\nНаименьшее последнее число : {1}", FirstPositive, LastNegative);
            
            //Task 2
            int n = instance.BiggerPositiveElement(ArrayOfNumbers, 5);
            Console.WriteLine(n);

            //Task 3
            string str = instance.LastElement(ArrayOfStrings, 1, 6);
            Console.WriteLine(str);

            //Task 5
            IEnumerable<string> words = instance.StartAndChar('а', ArrayOfStrings);

            foreach(var word in words)
                Console.WriteLine(word);

            //Task 6
            int sumAllStrings = instance.SumAllStrings(ArrayOfStrings);
            Console.WriteLine("Сумма длин всех строк = {0}",sumAllStrings);

            //Task 7
            int sumNegativeElements = instance.SumNegativeElements(ArrayOfNumbers);
            int quantityNegativeElement = instance.QuantityNegativeElements(ArrayOfNumbers);

            Console.WriteLine("Сумма негативных элементов = {0}, а их количество = {1}",sumNegativeElements,quantityNegativeElement);

            //Task 12
            
            int AgrNumber = instance.AgrLastNumbers(ArrayOfNumbers);
            Console.WriteLine("Произведение чисел = {0}",AgrNumber);

            //Task 13
            
            double sumRange = instance.SumRange(6);
            Console.WriteLine("Сумма = {0}",sumRange);

            //Task 15
            
            double averagePow = instance.AveragePow(3, 4);
            Console.WriteLine("Среднее квадрата = {0}",averagePow);

            //Task 18
            
            IEnumerable<int> reversNumbers = instance.ReversNumbers(ArrayOfNumbers);
			
            Console.Write("Чётные отрицательные ");
            foreach(var number in reversNumbers)
                	Console.Write(number + " ");


            Console.WriteLine();
            
            //Task 19
            
            IEnumerable<int> distinctsNumbers = instance.DistinctsNumbers(ArrayOfNumbers,3);
            
			Console.Write("Различные положительные ");
            foreach (var number in distinctsNumbers)
                Console.Write(number + " ");

            //Task 22
            
            List<string> sortedList = instance.SortedStirng(ArrayOfStrings, 3);
            sortedList.Sort();
            
            Console.WriteLine();

            foreach (var someStr in sortedList)
                Console.Write(someStr);

            //Task 27
            
            IEnumerable<int> ungeradeNumbers = instance.SortedUngerade(ArrayOfNumbers,2);
			
            Console.Write("Нечетные положительные ");
            foreach (var someDigits in ungeradeNumbers)
                Console.Write(someDigits + " ");

            //Task 32
            
            IEnumerable<char> reversSymbols = instance.ReversSymbols(ArrayOfStrings);

            Console.WriteLine();

            foreach (var someStr in reversSymbols)
                Console.Write(someStr + " ");

            
            
            Console.ReadKey();
        }
    }
    
    
    class LinqTasks
    {

        // Task 1

        public int FirstPositive (int[] arrNumbers)
        {
            return arrNumbers.Where(n => n > 0).First();
        }

        public int LastNegative (int[] arrNumbers)
        {
            return arrNumbers.Where(n => n < 0).Last();
        }



        // Task 2

        public int BiggerPositiveElement(int[] arrNumbers, int digit)
        {
            try
            {
                return arrNumbers.Where(n => n.ToString().EndsWith(digit.ToString())).Where(n => n > 0).First();
            }
            catch
            {
                return 0;
            }
        }

        
       // Task 3
       
        public string LastElement(string[] arrStrings, int digit, int length)
        {
            try
            {
                return arrStrings.Where(n => n.StartsWith(digit.ToString())).Where(n => n.Length == length).Last();
            }
            catch
            {
                return "Error";
            }
        }


        // Task 5
        
        public IEnumerable<string> StartAndChar(char c, string[] str)
        {
            var newStr = str.Where(n => n.Length > 1).Where(n => n.ToLower().StartsWith(c.ToString()) && n.ToLower().EndsWith(c.ToString()));
            return newStr;
        }
     

        //  Task 6
        
        public int SumAllStrings(string[] str)
        {
            var newStr = str.Sum(n => n.Length);
            return newStr;
        }
 

        // Task 7
        
        public int SumNegativeElements(int[] arrNumbers)
        {
            var newArrNumbers = arrNumbers.Where(n => n < 0).Sum();
            return newArrNumbers;
        }

        public int QuantityNegativeElements(int[] arrNumbers)
        {
            var quantity = arrNumbers.Where(n => n < 0).Count();
            return quantity;
        }
     

        //  Task 12
        
        public int AgrLastNumbers(int[] arrNumbers)
        {
            var newArrNumbers = arrNumbers.Aggregate((n, y) => n * y);
            return newArrNumbers;
        }
   

       	// Task 13
       	
        public double SumRange(int n)
        {
            if (n > 0)
                return Enumerable.Range(1, n).Sum(e => e + (e / n));
            else
                return 0.0;
        }
   

       	// Task 15
       	
        public double AveragePow(int a, int b)
        {
            if (b > a)
                return Enumerable.Range(a, b).Average(n => n * 2);
            else
                return 0.0;
        }
 

        // Task 18
        
        public IEnumerable<int> ReversNumbers(int[] arrNumbers)
        {
            var newArrNumbers = arrNumbers.Where(n => n % 2 == 0 && n < 0).Reverse();
            return newArrNumbers;
        }
   

        // Task 19
        
        public IEnumerable<int> DistinctsNumbers(int[] a, int d)
        {
            var newArrNumbers = a.Where(n => n.ToString().EndsWith(d.ToString())).Where(n => n > 0).Reverse().Distinct().Reverse();
            return newArrNumbers;
        }
  

        // Task 22
        
        public List<string> SortedStirng(string[] a, int k)
        {
            var newArrNumbers = a.Where(n => n.Length == k && k > 0 && char.IsDigit(Convert.ToChar(n.Substring(n.Length - 1)))).ToList();
            return newArrNumbers;
        }


        // Task 27
        
        public IEnumerable<int> SortedUngerade(int[] a, int d)
        {
            var newArrNumbers = a.Where(n => n > d && n % 2 != 0).Reverse();
            return newArrNumbers;
        }


        // Task 32
        
        public IEnumerable<char> ReversSymbols(string[] a)
        {
            var newArrNumbers = a.SelectMany(n => n).Reverse();
            return newArrNumbers;
        }
   
    }
}