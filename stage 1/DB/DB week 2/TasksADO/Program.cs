﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Data.SqlClient;
using System.Diagnostics;




namespace WeekFive
{
    class Program
    {
        static void Main(string[] args)
        {
            Tasks tasks = new Tasks();

            //tasks.Task_18();
            //tasks.Task_19();
            //tasks.Task_20();
            //tasks.Task_21();
            //tasks.Task_22();
            //tasks.Task_23();
            //tasks.Task_24();
            //tasks.Task_25();
            //tasks.Task_26();
            //tasks.Task_27();
            
            Console.ReadKey();
        }
    }
}



    class ManagerSqlConnection
    {
        private const string strConnection = @"Data Source=FELDMARCHALL-ПК\SQLEXPRESS;Initial Catalog=Northwind; Integrated Security=True; Pooling=False";

        private SqlConnection connection = new SqlConnection(strConnection);

        public void OpenConnection()
        {
            try
            {
                connection.Open();
            }
            catch(Exception exep)
            {
                Console.WriteLine(exep.Message);
            }
        }

        public SqlDataReader SqlCommand(string strCommand)
        {
            SqlDataReader dataReader = null;

            try
            {
                SqlCommand myCommand = new SqlCommand(strCommand, connection);

                dataReader = myCommand.ExecuteReader();

                return dataReader;
            }
            catch(Exception exep)
            {
                Console.WriteLine(exep.Message);
            }

            return dataReader;
        }

        public void CloseConnection()
        {
            try
            {
                connection.Close();
            }
            catch(Exception exep)
            {
                Console.WriteLine(exep.Message);
            }
        }
    }

    
    
class Tasks
    {
        ManagerSqlConnection connection = new ManagerSqlConnection();

        SqlDataReader command = null;

        public void Task_18()
        {
            int i = 0;

            connection.OpenConnection();
            command = connection.SqlCommand("SELECT ProductName,UnitsInStock FROM dbo.Products WHERE UnitsInStock > 10");

            while(command.Read())
                Console.WriteLine("{2}.Продукт - {0}, количество: {1}", command[0],command[1], ++i);

            command.Close();
            connection.CloseConnection();
        }

        public void Task_19()
        {
            int i = 0;

            connection.OpenConnection();
            command = connection.SqlCommand("SELECT C.ContactName,OD.Quantity,UnitPrice*OD.Quantity AS Sum FROM "+
                                            " Orders O JOIN dbo.[Order Details] OD ON " +
                                            " O.OrderID = OD.OrderID JOIN Customers C ON " +
                                            " C.CustomerID = O.CustomerID " +
                                            " WHERE OD.Quantity < 5 " +
                                            " ORDER By Sum");

            while (command.Read())
                Console.WriteLine("{3}.Клиент - {0}, количество: {1}, сумма: {2}", command[0], command[1], command[2],++i);

            command.Close();
            connection.CloseConnection();
        }

        public void Task_20()
        {
            int i = 0;

            connection.OpenConnection();
            command = connection.SqlCommand("SELECT C.ContactName,OD.Quantity,UnitPrice*OD.Quantity AS Sum FROM " +
                                            " Orders O JOIN dbo.[Order Details] OD ON" +
                                            " O.OrderID = OD.OrderID JOIN Customers C ON " +
                                            " C.CustomerID = O.CustomerID " +
                                            " WHERE C.ContactName Like'%_a_%'" +
                                            " ORDER By Sum");

            while (command.Read())
                Console.WriteLine("{2}.Продукт - {0}, количество: {1}", command[0], command[1], ++i);

            command.Close();
            connection.CloseConnection();
        }

        public void Task_21()
        {
            int i = 0;

            connection.OpenConnection();
            command = connection.SqlCommand("CREATE VIEW VwSelectDate " +
                                            "AS "+
                                            "SELECT O.OrderDate,OD.Quantity,OD.UnitPrice,P.ProductName From Orders O JOIN [Order Details] OD ON " +
                                            "O.OrderID = OD.OrderID JOIN Products P ON " +
                                            "OD.ProductID = P.ProductID " +
                                            "WHERE OrderDate < '1997' " +
                                            "WITH CHECK OPTION");
            command.Close();

            command = connection.SqlCommand("Select * FROM VwSelectDate " +
                                            "ORDER BY VwSelectDate.UnitPrice DESC");

            while (command.Read())
                Console.WriteLine("{4}.Продукт -{3} Дата заказа - {0}, количество: {1}, цена - {2}$", command[0], command[1], command[2], command[3], ++i);
            command.Close();

            connection.CloseConnection();
        }

        public void Task_22()
        {
            int i = 0;

            connection.OpenConnection();
            command = connection.SqlCommand("CREATE VIEW VwSelectDateTopSeven " +
                                            "with encryption " +
                                            "AS " +
                                            "SELECT TOP 7 O.OrderDate,C.ContactName,OD.UnitPrice*OD.Quantity as [Sum],P.ProductName From Orders O JOIN [Order Details] OD ON " +
                                            "O.OrderID = OD.OrderID JOIN Products P ON " +
                                            "OD.ProductID = P.ProductID JOIN Customers C ON " +
                                            "O.CustomerID = C.CustomerID ");
            command.Close();

            command = connection.SqlCommand("SELECT * FROM VwSelectDateTopSeven " +
                                            "ORDER BY VwSelectDateTopSeven.Sum ASC");

            while (command.Read())
                Console.WriteLine("{4}.Продукт -{3} Дата заказа - {0}, количество: {1}, сумма - {2}$", command[0], command[1], command[2], command[3], ++i);

            command.Close();
            connection.CloseConnection();
        }

        public void Task_23()
        {
            int i = 0;

            connection.OpenConnection();
            command = connection.SqlCommand("CREATE VIEW VwSelectEachProduct " +
                                            "AS " +
                                            "SELECT OD.UnitPrice*OD.Quantity as [Sum],P.ProductName From Orders O JOIN [Order Details] OD ON " +
                                            "O.OrderID = OD.OrderID JOIN Products P ON " +
                                            "OD.ProductID = P.ProductID ");
            command.Close();

            command = connection.SqlCommand("SELECT * FROM VwSelectEachProduct " +
                                            "Order by VwSelectEachProduct.Sum DESC");

            while (command.Read())
                Console.WriteLine("{2}.Продукт: {1}, сумма - {0}, ", command[0], command[1], ++i);

            connection.CloseConnection();
        }

        public void Task_24()
        {
            int i = 0;

            connection.OpenConnection();
            command = connection.SqlCommand("CREATE VIEW VwSelectProductQuantity " +
                                            "with encryption " +
                                            "AS " +
                                            "SELECT OD.UnitPrice*OD.Quantity as [Sum],P.ProductName From [Order Details] OD JOIN Products P ON " +
                                            "OD.ProductID = P.ProductID ");
            command.Close();

            command = connection.SqlCommand("SELECT * FROM VwSelectProductQuantity " +
                                            "Order by VwSelectProductQuantity.Sum DESC ");

            while (command.Read())
                Console.WriteLine("{2}.Продукт: {1}, сумма - {0}, ", command[0], command[1], ++i);

            connection.CloseConnection();
        }

        public void Task_25()
        {
            int i = 0;

            connection.OpenConnection();

            command = connection.SqlCommand("CREATE VIEW VwSelectEachProductForDate " +
                                            "AS " +
                                            "SELECT P.ProductName, OD.Quantity * OD.UnitPrice AS Sum FROM Products P JOIN [Order Details] OD ON " +
                                            "P.ProductID = OD.ProductID JOIN Orders O ON " +
                                            "O.OrderID = OD.OrderID " +
                                            "WHERE O.OrderDate < '1997-01-01' ");
            command.Close();

            command = connection.SqlCommand("SELECT * FROM VwSelectEachProductForDate " +
                                            "Order by VwSelectEachProductForDate.Sum DESC");

            while (command.Read())
                Console.WriteLine("{2}.Продукт: {0}, сумма - {1} ", command[0], command[1], ++i);

            connection.CloseConnection();
        }

        public void Task_26()
        {

            int i = 0;

            connection.OpenConnection();
            command = connection.SqlCommand("CREATE VIEW VwEachProductForDateTopSeven " +
                                            "AS " +
                                            "SELECT TOP 7 P.ProductName, OD.Quantity * OD.UnitPrice AS Sum FROM Products P JOIN [Order Details] OD ON " +
                                            "P.ProductID = OD.ProductID JOIN Orders O ON " +
                                            "O.OrderID = OD.OrderID " +
                                            "WHERE O.OrderDate < '1997-01-01'");
            command.Close();

            command = connection.SqlCommand("SELECT * FROM VwEachProductForDateTopSeven " +
                                            "Order by VwEachProductForDateTopSeven.Sum DESC");

            while (command.Read())
                Console.WriteLine("{2}.Продукт: {0}, сумма - {1} ", command[0], command[1], ++i);

            connection.CloseConnection();
        }

        public void Task_27()
        {

            int i = 0;

            connection.OpenConnection();
            command = connection.SqlCommand("CREATE VIEW VwEachCustomer " +
                                            "WITH ENCRYPTION " +
                                            "AS " +
                                            "SELECT C.ContactName AS Name,UnitPrice*OD.Quantity AS [Common sum] FROM  " +
                                            "Orders O JOIN [Order Details] OD ON " +
                                            "O.OrderID = OD.OrderID JOIN Customers C ON "+
                                            "C.CustomerID = O.CustomerID " +
                                            "WHERE UnitPrice*OD.Quantity > 500 " +
                                            "WITH CHECK OPTION");
            command.Close();

            command = connection.SqlCommand("SELECT * FROM VwEachCustomer " +
                                            "ORDER BY VwEachCustomer.[Common sum]  DESC");

            while (command.Read())
                Console.WriteLine("{2}.Продукт:  {0}, сумма - {1}  ", command[0], command[1], ++i);

            connection.CloseConnection();
        }
    }

