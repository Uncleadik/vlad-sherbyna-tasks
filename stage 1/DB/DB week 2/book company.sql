CREATE TABLE Authors(
Code_author int not null Primary key,
Name_author nvarchar(50) not null,
Birthday date not null
)

CREATE TABLE Publishing_house(
Code_publish int not null Primary key,
Publish nvarchar(50) not null,
City nvarchar(30) not null
)

CREATE TABLE Books(
Code_book int not null Primary key,
Title_book nvarchar(50) not null,
Code_author int not null,
Pages int not null,
Code_publish int not null,
Constraint FK_Books_Authors Foreign key (Code_author) References Publishing_house (Code_publish),
Constraint FK_Books_Publishing_house Foreign key (Code_publish) References Authors (Code_author)
)

CREATE TABLE Deliveries(
Code_delivery int not null Primary key,
Name_delivery nvarchar(50) not null,
Name_company nvarchar(50) not null,
Addres nvarchar(70) not null,
Phone int,
INN int not null
)

CREATE TABLE Purchases(
Code_book int,
Date_order date,
Code_delivery int,
Type_purchase nvarchar(50),
Cost money ,
Amout int ,
Code_purchase int,
Constraint FK_Purchases_Books Foreign key (Code_book) References Books (Code_book),
Constraint FK_Purchases_Deliveries Foreign key (Code_delivery) References Deliveries (Code_delivery))


INSERT INTO dbo.Authors VALUES
(1,'Sergey',  '2011-04-01'),
(2,'Andrey', '1995-10-15'), 
(3,'Ivan', '1987-01-30'), 
(4,'Vlad', '1974-12-02'), 
(5,'Vika', '1989-05-07');

INSERT INTO dbo.Books VALUES
(1,'Osvoboditel', 1, 206, 1),
(2,'Respublica', 2, 671, 2),
(3,'Napoleon', 3, 118, 3),
(4,'Delta', 4, 304, 4),
(5,'Hotel', 5, 228, 5);

INSERT INTO dbo.Publishing_house VALUES
(1,'first', 'Moscow'),
(2,'second', 'Odessa'),
(3,'first', 'Moscow'),
(4,'second', 'Lviv'),
(5,'first', 'Moscow')

INSERT INTO dbo.Deliveries VALUES
(1,'DelPost', 'PostInc', 'Marcow.st', 013261256, 3612),
(2,'fedEx', 'fInc', 'Resenburg.st', 058126125, 1261),
(3,'DelPost', 'PostInc', 'Marcow.st', 013261256, 3612),
(4,'DelPost', 'PostInc', 'Marcow.st', 013261256, 3612),
(5,'fedEx', 'fInc', 'Resenburg.st', 058126125, 1261),
(6,'DelPost', 'fInc', 'Resenburg.st', 36237734, 4237),
(7,'DelPost', 'fInc', 'Resenburg.st', 36237734, 4237),
(8,'DelPost', 'fInc', 'Resenburg.st', 36237734, 4237)

INSERT INTO dbo.Purchases VALUES
(1, '2016-08-27', 1, 'private', 1262, 63, 1),
(2, '2016-08-27', 2, 'public', 8231, 61, 2),
(3, '2016-08-27', 3, 'private', 3681, 12, 3),
(4, '2016-08-27', 4, 'private', 9214, 72, 4),
(5, '2016-08-27', 5, 'public', 1168, 63, 5)



use [Book company]

-- ������� INSERT � SELECT

-- ����������� ���������� ������ � ������� � �������������� �������� ��
-- ������ �������
-- !!!!!!!! �������� �������� NOT NULL � ������� Purchases, ������ ����� �������� �� ������ ��������!!!!!!! 

INSERT INTO Purchases(Code_delivery)
  SELECT Code_delivery
    FROM Deliveries
    WHERE Code_delivery>5;

-- ��������� ��������� ������ SELECT ��� ������� �������� ������ ��� �����
-- ��� ���������� ����� ����������� ������� ����� � ������� (� ������������
-- ���������� ����������� �����)

INSERT INTO Purchases(Code_delivery)
  SELECT top 2 Code_delivery
    FROM Deliveries
	WHERE Code_delivery>5;



-- �������� ����� � ������� ���������� DELETE

-- ���������� ������ �������� ����� ��������� ����� ��������� ����� � ������� ��������� �����
delete top(3) from Books


-- ���������� ����� � ������� ���������� UPDATE

-- ���������� ���������� ���������� ��������
UPDATE Purchases SET Cost = Cost*0.9, 
Date_order = '2015-03-15' where Code_book = 2


-- ���������� ���������� ������������� ���������� �����
-- (����������� TOP), �� ������� ������ ���������� UPDATE
UPDATE top (3) Purchases SET Cost = Cost*2, 
Date_order = '2015-03-15' where Code_book = 2 
