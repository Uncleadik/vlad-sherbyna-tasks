CREATE DATABASE Firm
Use Firm

Create table providers (
[NumPost] char(5) NOT NULL,
[Name] char(20),
[Rating] smallint,
[Town] char(15)
)

Create table components (
[NumCom] char(6) NOT NULL,
[Name] char(20),
[Color] char(20),
[Weight] smallint,
[Town] char(15)
)

Create table deliverys (
[NumPost] char(5) NOT NULL,
[NumCom] char(6),
[Date_post] date,
[Sum] smallint
)

Insert into providers Values 
	(1, 'Smith', 20, 'London'),
	(2, 'John', 10, 'Paris'),
	(3, 'Blaic', 30, 'Paris'),
	(4, 'Clarc', 20, 'London'),
	(5, 'Adams', 30, 'Atens');

Insert into components Values 
	(1, 'Nut', 'Red', 12, 'London'),
	(2, 'Bolt', 'Green', 17, 'Paris'),
	(3, 'Screw', 'Blue', 17, 'Rome'),
	(4, 'Screw', 'Red', 14, 'London'),
	(5, 'Jaw', 'Blue', 12, 'Paris'),
	(6, 'Bloom', 'Red', 19, 'London');


Insert into deliverys Values 
	(1, 1, '1995-02-01', 300),
	(1, 2, '1995-04-05', 200),
	(1, 3, '1995-05-12', 400),
	(1, 4, '1995-06-15', 200),
	(1, 5, '1995-07-22', 100),
	(1, 6, '1995-08-12', 100),
	(2, 1, '1995-03-03', 300),
	(2, 2, '1995-06-12', 400),
	(3, 2, '1995-04-04', 200),
	(4, 2, '1995-03-23', 200),
	(4, 4, '1995-06-17', 300),
	(4, 5, '1995-08-22', 400);