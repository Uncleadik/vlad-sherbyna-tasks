use Firm

-- ������ ������ ���������� � �����������
select * from providers

-- ������ ������� S � ��������� �������: �������, �����, �������, �����_����������
select Name, Town, Rating, NumPost from providers

-- ������ ������ ���� ������������ �������.
select NumCom from components

-- ������ ������ ���� ������������ �������, �������� ������������.
select distinct NumCom from components

-- ������ ���������� ������� �� ���� ���� � ������� ����������.
select SUBSTRING(Name, 1, 2) as Name, Rating from providers

-- ������ ������ �����������, ������������� �� ������, � �������� ������ - �� ��������.
select * from providers
order by Town, Rating

-- ������� ������ �������, ������������ � ����� ���
select * from components
where Name like 'B%'

-- ������ �������, ����������� � ������������ ����� �������� ��� ���������� S1 � ��������������� ����������
select AVG(Sum) as average, MIN(Sum) as minamum, MAX(Sum) as maximum
from deliverys
where NumPost = 1

-- ������ �������� �������� � �� ����������, � ����� ����, �����, ���� ������ � ���������� ����,
-- ��������� � ������� �������� �� ����������� ����.
select NumCom, Sum,
    DATEPART(day, Date_post) as Day, 
	DATEPART(month, Date_post) as mounth, 
    DATEPART(weekday, Date_post) as weekday
from deliverys  

-- ������ ��� ���������� ���������� � ����������� � �������, ������������� � ����� ������
select * from providers inner join
 components on providers.Town = components.Town

-- ������ ��� ������ ������������ ������ �� ����� � ����� ����� ��������, �� ����������� ��������
-- ���������� S1
select NumCom, sum(Sum)
from deliverys
where NumPost <> 1
group by NumCom

--������ ������ �������, ������� ����� ��� ����� 16 ������, ���� ������������ ����������� S2.
select NumCom
from components
where Weight > 16
union
select NumCom
from deliverys
where NumPost = 2

-- ������� ��� �������� ��� ����������� �� �������.
-- ���������: ������� SP � �������������� �������� � ��������� ��� ����������� �� �������
delete from providers
where Town = 'London'

