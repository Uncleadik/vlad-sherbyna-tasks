use Northwind

-- 1. �������� ������ �� ������� ���� ������� � ����� � ��������� [15,30).
select UnitPrice from Products
where UnitPrice between 15 and 30

-- 2. �������� ������ �� ������� ���� ������� ������������ �� ��������� ����� ��.
select ProductName from Products
where ProductName like 'C%'

-- �������� ������ �� ������� ���� ������� ������� � �������� ������ ��������� ����� �b�.
select ProductName from Products
where ProductName like '_b%'

-- �������� ������ �� ������� ���� �������, � �������� ������� ����������� ������������������ �eso�.
select ProductName from Products
where ProductName like '%eso%'

-- �������� ������ �� ������� ���� �������, �������� ������� ������������� �� �es�
select ProductName from Products
where ProductName like '%es'

-- �������� ������ �� ������� ������� � ��������. (����������� � ����� �����, ����������, ���� �������)
select ProductName, UnitsInStock, UnitPrice from Products

-- �������� ������ �� ������� ������� � ��������. (����������� � ����� �����, ����������,
--  ���� �������, ���� �������, ����������)
select ProductName, ShipVia, Freight, OrderDate, CustomerID
from Products, Orders

-- �������� ������ �� ������� ������� � ��������. (����������� � ����� �����, ����������, 
-- ���� �������, ���� �������, ����������). ��������� ������������ �� 
-- ������������ ������, ������ ����������.
select ProductName, ShipVia, Freight, OrderDate, CustomerID
from Products, Orders
order by ProductName

-- �������� ������ �� ������� ������� � ��������, �� 1997 ���. (����������� � ����� �����,
-- ����������, ���� �������, ���� �������). ��������� ������������ �� ����, �������� ����������
select ProductName, ShipVia, Freight, OrderDate, CustomerID
from Products, Orders
where DATEPART(YEAR, OrderDate) = 1997
order by Freight desc

-- �������� ������ �� ������� ������� � ��������, ��������� �� 1997 ����. (����������� � ����� �����,
-- ����������, ���� �������, ���� �������). ��������� ������������ �� ����, �������� ����������
select ProductName, ShipVia, Freight, OrderDate, CustomerID
from Products, Orders
where DATEPART(YEAR, OrderDate) < 1997
order by Freight desc

-- �������� ������ �� ������� ������ 7 ������� � ��������. (����������� � ����� �����,
-- �����, ���� �������, ����������). ��������� ������������ �� ����, ������ ����������.
select top 7 ProductName, ShipVia, Freight, OrderDate, CustomerID
from Products, Orders 
order by Freight

-- �������� ������ �� ������� ���� ������� � ��������� �������� ������� ������. 
-- (����������� � ����� �����, �����). ��������� ������������ �� �����, �������� ����������
SELECT Products.ProductName,
Sum(Products.UnitPrice * [Order Details].Quantity)
AS �����
FROM Products INNER JOIN [Order Details]
ON Products.ProductID = [Order Details].ProductID
GROUP BY Products.ProductName
order by Sum(Products.UnitPrice * [Order Details].Quantity) desc

-- �������� ������ �� ������� ���� ������� � ���������� ������ ������� ������. 
-- (����������� � �����, ����������). ��������� ������������ �� �����, �������� ����������
SELECT Products.ProductName,
Count([Order Details].Quantity)
AS ����������
FROM Products INNER JOIN [Order Details]
ON Products.ProductID = [Order Details].ProductID
GROUP BY Products.ProductName 
order by Count([Order Details].Quantity) desc

-- �������� ������ �� ������� ���� ������� � ��������� �������� ������� ������ �� 1997 ���.
-- (����������� � ����� �����, �����). ��������� ������������ �� �����, �������� ����������.
SELECT Products.ProductName,
Sum(Products.UnitPrice * [Order Details].Quantity)
AS �����
FROM Products INNER JOIN [Order Details]
ON Products.ProductID = [Order Details].ProductID
GROUP BY Products.ProductName
order by Sum(Products.UnitPrice * [Order Details].Quantity) desc

-- �������� ������ �� ������� ������ 8 ������� � ��������� �������� ������� ������ �� 1997 ���.
-- (����������� � ����� �����, �����). ��������� ������������ �� �����, �������� ����������
SELECT top 8 Products.ProductName,
Sum(Products.UnitPrice * [Order Details].Quantity)
AS �����
FROM Products INNER JOIN [Order Details]
ON Products.ProductID = [Order Details].ProductID
GROUP BY Products.ProductName
order by Sum(Products.UnitPrice * [Order Details].Quantity) desc

-- �������� ������ �� ������� ���� ������� � ��������� �������� ������� ����������,
-- �� ����� ������� 500. (����������� � ����������, �����). ��������� ������������ �� �����,
-- �������� ����������.
SELECT Orders.CustomerID,
Sum([Order Details].UnitPrice * [Order Details].Quantity)
AS �����
FROM Orders INNER JOIN [Order Details]
ON Orders.OrderID = [Order Details].OrderID
GROUP BY Orders.CustomerID
having Sum([Order Details].UnitPrice * [Order Details].Quantity) > 500
order by Sum([Order Details].UnitPrice * [Order Details].Quantity) desc

-- �������� ������ �� ������� ���� ������� � ��������� �������� ������� ����������, 
-- �� ����� ������� 1 000. (����������� � ����������, �����). ��������� ������������ �� �����,
-- �������� ����������
SELECT Orders.CustomerID,
Sum([Order Details].UnitPrice * [Order Details].Quantity)
AS �����
FROM Orders INNER JOIN [Order Details]
ON Orders.OrderID = [Order Details].OrderID
GROUP BY Orders.CustomerID
having Sum([Order Details].UnitPrice * [Order Details].Quantity) < 1000
order by Sum([Order Details].UnitPrice * [Order Details].Quantity) desc